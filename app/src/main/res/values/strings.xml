<resources>
    <string name="app_name">CM-NPM</string>

    <string name="permission_manager_service_description">
        Este serviço é responsável por mostrar a janela de pedido de permissões por parte das aplicações.
        Ao mesmo tempo, recolhe a resposta do utilizador e respetivos dados do contexto.
    </string>

    <string name="no_connection_warning">Sem conexão à internet. Por favor, ligue o wifi ou os dados móveis.</string>

    <!-- Foreground notification -->
    <string name="running_status_notification_content_title">Naive Permission Manager</string>
    <string name="running_status_notification_content_text">A correr em background.</string>
    <string name="running_status_notification_channel_name">Running Status Notification</string>
    <string name="running_status_notification_channel_description">Running Status Notification</string>

    <!-- User Activity Notification -->
    <string name="user_activity_notification_content_title">Aviso de Inatividade</string>
    <string name="user_activity_notification_content_text">Não tem respondido aos pedidos de permissões!</string>
    <string name="user_activity_notification_content_big_text">Se não usar o smartphone da campanha será desqualificado da campanha e não receberá o voucher.</string>
    <string name="user_activity_notification_channel_name">User Activity Notification</string>
    <string name="user_activity_notification_channel_description">User Activity Notification</string>

    <!-- Permission Prompt -->
    <string name="permission_request_header">Naive Permission Manager</string>
    <string name="deny_button">Negar</string>
    <string name="grant_button">Permitir</string>
    <string name="insert_semantic_location_title">Indique a sua localização atual:</string>
    <string name="home_toggle">Casa</string>
    <string name="work_toggle">Trabalho</string>
    <string name="travelling_toggle">Em viagem</string>
    <string name="other_toggle">Outra</string>
    <string name="expected_request_title">Para o que está a fazer no smartphone, esperava este pedido de acesso?</string>
    <string name="no_button">Não</string>
    <string name="yes_button">Sim</string>
    <string name="confirm_button">Confirmar</string>
    <string name="unanswered_permission_requests_message">Tem de permitir ou negar todas as permissões pedidas pela aplicação antes de confirmar.</string>
    <string name="unanswered_semantic_location_message">Tem de selecionar a sua localização atual antes de confirmar.</string>
    <string name="unanswered_was_request_expected_message">Deve selecionar se considera este pedido expectável, considerando a sua atual utilização do dispositivo.</string>

    <!-- Permission Descriptions. Slightly adapted from frameworks/base/core/res/res/values-pt-rPT -->
    <string name="permgrouprequest_contacts">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; aceda aos seus contactos?</string>
    <string name="permgrouprequest_location">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; aceda à sua localização?</string>
    <string name="permgrouprequest_calendar">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; aceda ao seu calendário?</string>
    <string name="permgrouprequest_sms">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; envie e veja mensagens SMS?</string>
    <string name="permgrouprequest_storage">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; aceda a fotos, media e ficheiros no dispositivo?</string>
    <string name="permgrouprequest_microphone">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; grave áudio (microfone)?</string>
    <string name="permgrouprequest_activityRecognition">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; aceda à sua atividade física</string>
    <string name="permgrouprequest_camera">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; tire fotos e grave vídeos</string>
    <string name="permgrouprequest_calllog">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; aceda aos registos de chamadas do seu telemóvel?</string>
    <string name="permgrouprequest_phone">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; faça e gira chamadas telefónicas?</string>
    <string name="permgrouprequest_sensors">Pretende permitir que a aplicação &lt;b&gt;%1$s&lt;/b&gt; aceda aos dados do sensor acerca dos seus sinais vitais?</string>

    <!-- Setup npm activity -->
    <string name="activity_setup_intro">Desculpe pela interrupção… O Naive Permission Manager (CM-NPM) necessita de algumas permissões em falta para poder funcionar corretamente.
        \nEm princípio este setup só é necessário uma vez. Siga as instruções abaixo e em caso de dúvida contacte-nos para cop-mode@dei.uc.pt.
    </string>
    <string name="activity_setup_required_permissions_info">
        Algumas permissões ainda não foram dadas ao CM-NPM. Deverá permitir todas as permissões
        para que possamos coletar todos os dados necessários.
        \nClique no botão abaixo para pedir as permissões e, seguidamente, clique em permitir em todas as permissões que forem pedidas.
    </string>
    <string name="request_permissions_button">Pedir Permissões</string>
    <string name="activity_setup_required_package_usage">
        O CM-NPM precisa da permissão \'Acesso de Utilização\' para poder captar as aplicações que correm em segundo plano aquando um pedido de permissões.
        Esta permissão tem de ser explicitamente dada pelo utilizador nas definições do sistema. Para
        tal precisamos que clique no botão abaixo para abrir as definições de \'Acesso de utilização\'.
        Na lista de aplicações que deverá aparecer, deverá procurar e clicar na CM-NPM, seguido
        de um clique sobre \'Permitir acesso de utilização\'. Pode seguidamente fechar as definições.
    </string>
    <string name="request_package_usage_button">Abrir Definições</string>
    <string name="request_package_usage_not_yet_confirmed">Tem de permitir o Acesso de Utilização para que o CM-NPM funcione corretamente.</string>
    <string name="activity_setup_always_scan_required">
        Para que o CM-NPM possa constantemente recolher os dispositivos na sua proximidade (através do wifi), precisamos que
        permita que o CM-NPM possa verificar o wifi mesmo quando este esteja desligado. Clique no botão abaixo para permitir este acesso.
    </string>
    <string name="request_always_scan_button">Permitir scans wifi</string>
    <string name="status_header">Estado: </string>
    <string name="package_usage_permission_not_granted">Ainda não deu a permissão \'Acesso de Utilização\'.</string>
    <string name="some_permissions_not_granted">Ainda não permitiu todas as permissões necessárias.</string>
    <string name="always_scan_not_granted">Ainda não permitiu os scans wifi.</string>
    <string name="follow_instructions_below">Por favor, siga as instruções abaixo para concluir o setup.</string>
    <string name="all_permissions_granted">Completo, obrigado. Pode agora sair desta aplicação clicando no botão abaixo. O CM-NPM continuará a correr em background.</string>
    <string name="close_setup_button">Sair</string>

    <!-- Main Activity -->
    <string name="mainactivity_actionbar_title">Naive Permission Manager</string>
    <string name="permission_history_title">Histórico de Permissões</string>
    <string name="menu_item_uploads">Uploads</string>
    <string name="menu_item_about">Sobre</string>
    <string name="menu_item_statistics">Estatísticas</string>
    <string name="dont_know_button">Não Sei</string>
    <string name="help_decision_title">Decisão</string>
    <string name="decision_allowed">Permitido</string>
    <string name="decision_denied">Negado</string>
    <string name="help_decision_type_title">Tipo de Decisão</string>
    <string name="decision_type_user">Utilizador</string>
    <string name="decision_type_cache">Cache</string>
    <string name="decision_type_timeout">Timeout\n(expirou sem resposta)</string>
    <string name="decision_type_dismissed">Dispensado\n(terminado sem resposta)</string>
    <string name="close_help_button">Fechar</string>

    <!-- About Activity -->
    <string name="about_actionbar_title">CM-NPM: Sobre</string>
    <string name="about_content">
        O CM-NPM (COP-MODE Naive Permission Manager) é tanto um gestor de permissões como uma ferramenta de coleta de dados para o projeto COP-MODE.
        Se quiser saber mais sobre o projeto consulte o nosso site https://cop-mode.dei.uc.pt/. Esta página \'Sobre\' detalha apenas o CM-NPM.\n\n

        O CM-NPM está constantemente a correr, como indicado por uma notificação sempre visível, de modo a monitorizar o comportamento das outras aplicações.
        Quando uma aplicação (app) tenta aceder a uma permissão considerada sensível (categorização feita pelo sistema Android), o CM-NPM interceta o acesso e trata de perguntar
        ao utilizador se o permite, ou o nega. Aquando esta interceção, o CM-NPM coleta também os dados referentes ao estado do smartphone.
        As decisões de permitir ou negar por parte do utilizador são guardadas durante algum tempo, para que a aplicação possa aceder livremente à pedida permissão sem que o utilizador
        intervenhe constantemente.\n\n

        Os dados coletados serão esporadicamente enviados para o servidor do COP-MODE. Estes dados serão analisados pela nossa equipa para o desenvolvimento de um
        gestor de privacidade automático, personalizado e que atua de acordo com o contexto tanto do utilizador como do dispositivo.\n\n

        Ao abrir a aplicação CM-NPM será apresentado um histórico de decisões de permissões. Nessa página poderá clicar no ícone de ajuda, situado no canto superior direito,
        para obter uma legenda.\n\n

        Quaisquer dúvidas que possa ter durante a campanha de recolha de dados poderá contactar-nos por email para: cop-mode@dei.uc.pt.
    </string>

    <!-- Uploads Activity-->
    <string name="uploads_actionbar_title">CM-NPM: Uploads</string>
    <string name="uploads_description">O CM-NPM tentará enviar os dados para o servidor ocasionalmente.
        Este upload poderá falhar por falta de conexão tanto do smartphone como do servidor.
        Portanto, poderá clicar no botão abaixo para iniciar o upload manualmente.
    </string>
    <string name="uploads_last_upload_date_header">Último upload:</string>
    <string name="uploads_upload_button">Enviar</string>
    <string name="uploads_data_to_send_header">Dados por enviar:</string>
    <plurals name="uploads_has_data">
        <item quantity="one">%d permissão por enviar</item>
        <item quantity="other">%d permissões por enviar</item>
    </plurals>
    <string name="uploads_has_no_data">Sem dados</string>
    <string name="uploads_never_uploaded">nunca</string>
    <string name="uploads_error_uploading">
        O upload falhou inesperadamente. Por favor, tente mais tarde. Se o erro persistir,
        contacte-nos para cop-mode@dei.uc.pt\n\nErro: %s
    </string>

    <!-- Statistics Activity -->
    <string name="statistics_actionbar_title">CM-NPM: Estatísticas</string>
    <string name="statistics_intro">Abaixo apresentamos o número de pedidos de permissões por tipo de decisão nas últimas 24 horas e desde o início da campanha.
        \n\n<b>Nota:</b> Se não usar o smartphone da campanha ativamente será desqualificado da campanha e não receberá o voucher.</string>
    <string name="decision_type_header">Tipo de Decisão</string>
    <string name="daily_header">24 Horas</string>
    <string name="always_header">Sempre</string>
    <string name="phone_id_header">Phone ID:</string>

</resources>
