package pt.uc.dei.copmode.npm.receivers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;

import androidx.annotation.NonNull;

import pt.uc.dei.copmode.npm.components.ContextualDataHolder;

/**
 * Because the intent CONNECTIVITY_ACTION is now deprecated as of API lvl 28, we implement this as a Network callback
 * See: https://developer.android.com/reference/android/net/ConnectivityManager#CONNECTIVITY_ACTION
 * The callback solution requires API level 24. Because I have not time, I won't implement the broadcast solution for api level 23.
 * So I bumped the project's minSdk to 24, which is fine as our phones are api level 28.
 */
public class ConnectivityReceiver implements ContextReceiver {
    private static String TAG = "ConnectivityReceiver";
    private boolean isReceiving = false;

    @Override
    public boolean registerSelf(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        // we register the default network to have callbacks only for the active network.
        connMgr.registerDefaultNetworkCallback(new MyNetworkCallback(context));
        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }

    private static class MyNetworkCallback extends ConnectivityManager.NetworkCallback {
        private Context mContext;

        MyNetworkCallback(@NonNull Context ctx) {
            mContext = ctx;
        }

        @Override
        public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities networkCapabilities) {
            // This is also called when new network connects.
//            MyLogHelper.logDebug(TAG, "onCapabilitiesChanged: network " + network.toString() + " is metered? "
//                    + !networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED));
            ContextualDataHolder.getInstance(mContext).updateNetwork(networkCapabilities);
        }

        @Override
        public void onLost(@NonNull Network network) {
//            MyLogHelper.logDebug(TAG, "Lost connection to network " + network.toString());
            ContextualDataHolder.getInstance(mContext).updateNetwork(null);
        }
    }


}
