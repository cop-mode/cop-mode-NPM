package pt.uc.dei.copmode.npm.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import pt.uc.dei.copmode.npm.components.ContextualDataHolder;

public class BatteryReceiver extends BroadcastReceiver implements ContextReceiver {
    private static final String TAG = "BatteryReceiver";
    private boolean isReceiving = false;
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null || !action.equals(Intent.ACTION_BATTERY_CHANGED))
            return;
        // MyLogHelper.logDebug(TAG, "Received battery changed");
        int plugState = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        ContextualDataHolder.getInstance(context).updatePlugState(plugState);
    }

    @Override
    public boolean registerSelf(Context context) {
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);  // we can get more info with this one
        // IntentFilter filter = new IntentFilter(BatteryManager.ACTION_CHARGING);
        // filter.addAction(BatteryManager.ACTION_DISCHARGING);
        context.registerReceiver(this, filter);
        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }
}
