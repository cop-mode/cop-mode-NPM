package pt.uc.dei.copmode.npm.backend.converters;

import androidx.room.TypeConverter;

import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;

public class PermissionDialogAnswerTypeConverter {
    @TypeConverter
    public static PermissionDialogAnswerType toNetworkStatus(int value) {
        return PermissionDialogAnswerType.fromValue(value);
    }

    @TypeConverter
    public static Integer toInteger(PermissionDialogAnswerType permissionDialogAnswerType) {
        return permissionDialogAnswerType.getValue();
    }
}
