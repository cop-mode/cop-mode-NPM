package pt.uc.dei.copmode.npm.backend.entities;

public enum PermissionDialogAnswerType {
    UNANSWERED(0),
    USER_ANSWERED(1),
    CACHE_ANSWERED(2),
    TIMEDOUT(3),
    DISMISSED(4),  // to differentiate between instances that were not yet prompted from dismissed instances
    UNHANDLED(5);  // because the service takes some time to be bound and we can't lock the main thread, some permission checks will be missed.

    private int value;
    public int getValue() {
        return value;
    }

    PermissionDialogAnswerType(int value) {
        this.value = value;
    }

    public static PermissionDialogAnswerType fromValue(int value) {
        switch (value) {
            case 0:
                return UNANSWERED;
            case 1:
                return USER_ANSWERED;
            case 2:
                return CACHE_ANSWERED;
            case 3:
                return TIMEDOUT;
            case 4:
                return DISMISSED;
            case 5:
                return UNHANDLED;
            default:
                throw new RuntimeException("Attempted to get PermissionDialogAnswerType from invalid value = " + value);
        }
    }
}
