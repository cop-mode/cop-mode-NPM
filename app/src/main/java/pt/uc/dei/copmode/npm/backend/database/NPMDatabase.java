package pt.uc.dei.copmode.npm.backend.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import pt.uc.dei.copmode.npm.backend.converters.ArrayRunningApplicationInfoConverter;
import pt.uc.dei.copmode.npm.backend.converters.PermissionDialogAnswerTypeConverter;
import pt.uc.dei.copmode.npm.backend.entities.ContextData;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;

@Database(entities = {PermissionDialogData.class, ContextData.class, }, version = 1)
@TypeConverters({PermissionDialogAnswerTypeConverter.class, ArrayRunningApplicationInfoConverter.class})
public abstract class NPMDatabase extends RoomDatabase {
    private static volatile NPMDatabase instance;

    public static NPMDatabase getDB(Context context) {
        if (instance == null) {
            synchronized (NPMDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context, NPMDatabase.class, "npm-db").build();
                }
            }
        }
        return instance;
    }

    public abstract PermissionDialogDataDao permissionDialogDataDao();
    public abstract ContextDataDao contextDataDao();

}
