package pt.uc.dei.copmode.npm.frontend.activities;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.view.GravityCompat;
import androidx.core.widget.TextViewCompat;

import java.util.ArrayList;
import java.util.List;

import pt.uc.dei.copmode.npm.R;
import pt.uc.dei.copmode.npm.backend.database.NPMDatabase;
import pt.uc.dei.copmode.npm.backend.database.PermissionDialogDataDao;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

public class StatisticsActivity extends AppCompatActivity {
    private static String TAG = "StatisticsActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.statistics_actionbar_title);
        }

        fillStats();
    }

    private void fillStats() {
        // because we will access the DB, we do it on a new thread
        new Thread() {
            @Override
            public void run() {
                final TableLayout tableLayout = findViewById(R.id.statistics_data_table_layout);
                Resources res = getResources();

                PermissionDialogAnswerType[] answerTypes = PermissionDialogAnswerType.values();
                final List<String[]> dataRows = new ArrayList<>(answerTypes.length + 1);  // for the header

                // The data is to be display in rows of 3 elements. The first row is the head
                dataRows.add(new String[] {res.getString(R.string.decision_type_header), res.getString(R.string.daily_header), res.getString(R.string.always_header)});

                PermissionDialogDataDao dao = NPMDatabase.getDB(StatisticsActivity.this).permissionDialogDataDao();
                long yesterdayTimestamp = System.currentTimeMillis() - 24*3600000;
                for (PermissionDialogAnswerType answerType : answerTypes) {
                    if (answerType == PermissionDialogAnswerType.UNHANDLED || answerType == PermissionDialogAnswerType.UNANSWERED)
                        continue;

                    int countLast24hours = dao.countPermissionDialogDataWithAnswerTypeSince(answerType, yesterdayTimestamp);
                    int countAll = dao.countPermissionDialogDataWithAnswerTypeSince(answerType, 0);

                    String translatedAnswerType = translateAnswerType(answerType);
                    dataRows.add(new String[]{translatedAnswerType, String.valueOf(countLast24hours), String.valueOf(countAll)});
                }

                // update table in main loop
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        boolean isFirstRow = true;
                        for (String[] data: dataRows) {
                            addRow(tableLayout, data, isFirstRow);
                            isFirstRow = false;
                        }
                    }
                });
            }
        }.start();
    }

    private String translateAnswerType(PermissionDialogAnswerType answerType) {
        switch (answerType) {
            case USER_ANSWERED:
                return getString(R.string.decision_type_user);
            case CACHE_ANSWERED:
                return getString(R.string.decision_type_cache);
            case TIMEDOUT:
                return getString(R.string.decision_type_timeout);
            case DISMISSED:
                return getString(R.string.decision_type_dismissed);
            default:
                MyLogHelper.logError(TAG, "Wrong answer type (" + answerType + ") sent for translation in " + TAG);
                return answerType.toString();
        }
    }

    private void addRow(TableLayout tableLayout, String[] rowFields, boolean isHeader) {
        TableRow row = new TableRow(this);
        TableLayout.LayoutParams rowParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(rowParams);
        int fieldMargin = getResources().getInteger(R.integer.stats_field_margin);
        // row.setPadding(fieldMargin, fieldMargin, fieldMargin, fieldMargin);

        for(String fieldString: rowFields) {
            TextView headerTextView = new TextView(this);
            headerTextView.setText(fieldString);
            if (isHeader) {
                headerTextView.setTypeface(null, Typeface.BOLD);
            }
            // headerTextView.setBackgroundColor(R.color.colorAccent);
            headerTextView.setPadding(fieldMargin, fieldMargin, fieldMargin, fieldMargin);
            headerTextView.setGravity(Gravity.CENTER);
            row.addView(headerTextView);
        }
        tableLayout.addView(row);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
