package pt.uc.dei.copmode.npm.xposed;

import android.app.Activity;
import android.app.AndroidAppHelper;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;

import androidx.annotation.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.services.PermissionManagerService;

public class XRequestPermissionsHookHandler extends XC_MethodHook {
    private static String TAG = "xposed/XRequestPermissionsHookHandler";

    private static final XOnRequestPermissionsResultHookHandler xOnRequestPermissionsResultHookHandler = new XOnRequestPermissionsResultHookHandler();
    private String packageName;

    @Override
    protected void beforeHookedMethod(XC_MethodHook.MethodHookParam param) {
        // final Context application = AndroidAppHelper.currentApplication();
        Context applicationContext = AndroidAppHelper.currentApplication().getApplicationContext();
        packageName = applicationContext.getPackageName();
        XposedLogger.logDebug(TAG, packageName, "In XRequestPermissionsHookHandler");

        final Object thisObject; // requestPermissions must always be done from a UI context (Activity or Fragment)
        final String[] requestingPermissions;
        final int requestCode;

        if (param.args.length == 2) {
            thisObject = param.thisObject;
            requestingPermissions = (String[]) param.args[0];
            requestCode = (int) param.args[1];
        } else {
            // static methods have a null thisObject and these requestPermissions methods have an activity or fragment as first param
            thisObject = param.args[0];
            requestingPermissions = (String[]) param.args[1];
            requestCode = (int) param.args[2];
        }

        Method onRequestPermissionsResultMethod = XposedHelpers.findMethodExactIfExists(
                thisObject.getClass().getName(), thisObject.getClass().getClassLoader(),
                "onRequestPermissionsResult", "int", String[].class, "int[]");
        if (onRequestPermissionsResultMethod == null) {
            XposedLogger.logError(TAG, packageName, "Failed to obtain onRequestPermissionsResult method from thisObject");
            return;
        }

        // todo: check if we are hooking multiple times unnecessarily.
        XposedLogger.logDebug(TAG, packageName, "Hooking onRequestPermissionsResult");
        XposedBridge.hookMethod(onRequestPermissionsResultMethod, xOnRequestPermissionsResultHookHandler);

//        if (requestCode < 0) {
//            return;  // we let the default handler handle this error case
//        }
//
//        new RequestHandlerThread(thisObject, requestCode, requestingPermissions, applicationContext).start();
//
//        param.setResult(null);  // prevent the call to the original method, since we handle it completely



//
//
//        // get method onRequestPermissionsResult(int, java.lang.String[], int[])
//        Method onRequestPermissionsResult = XposedHelpers.findMethodExactIfExists(
//                thisObject.getClass().getName(), thisObject.getClass().getClassLoader(),
//                "onRequestPermissionsResult", "int", String[].class, "int[]"
//        );
//
//        if (onRequestPermissionsResult == null) {
//            MyLogHelper.logError(TAG, "Failed to get method onRequestPermissionsResult. Aborting hook...");
//            return;  // because we fail to handle, we allow the normal permission manager to be called
//        }
//
//        // Now we need to show the permission prompt.
//        // I was able to show the prompt from from hooked application, but it has the drawback that
//        // the layout must be fully hardcoded and imported (see permissionDialogHelper). The alternative
//        // that would solve this problem would be to call our service to prompt the permission request
//
//        final Activity thisObjectActivity = getThisObjectActivity(thisObject);
//
//        if (activitiesWithCurrentPermissionRequest.contains(thisObjectActivity.getClass().getName())) {
//            // just as android, we handle a single permission request at a time
//            MyLogHelper.logWarn(TAG, "A context can request only one set of permissions at a time");
//            try {
//                // Dispatch the callback with empty arrays which means a cancellation.
//                onRequestPermissionsResult.invoke(thisObject, requestCode, new String[0], new int[0]);
//                param.setResult(null);
//                return;
//            } catch (IllegalAccessException | InvocationTargetException e) {
//                MyLogHelper.logError(TAG, "Failed to invoke onRequestPermissionsResult. This should not happen!");
//                return;  // because we fail to handle, we allow the normal permission manager to be called
//            }
//        }
//
//        activitiesWithCurrentPermissionRequest.add(thisObjectActivity.getClass().getName());
//
////        new Handler(thisObjectActivity.getMainLooper()).post(new Runnable() {
////            // get the main ui thread, as requestPermissions might be called from a different thread
////            @Override
////            public void run() {
////                PermissionDialogHelper permissionDialog = new PermissionDialogHelper(thisObjectActivity, requestCode, requestingPermissions, XRequestPermissionsHookHandler.this);
////                permissionDialog.showPermissionPrompt();
////            }
////        });
//
//        // TODO Comunicate to the service the results of the prompt
//        // ((Context) thisObject).bindService()
//
//        param.setResult(null);
    }

    private static class RequestHandlerThread extends Thread {

        private final Object uiObject;
        private final int requestCode;
        private String[] requestingPermissions;
        private final Context applicationContext;
        private final String packageName;

        private HandlerThread handlerThread;  // async thread to receive the results from the service
        private MyServiceConnection mConnection;
        private CountDownLatch bindingCountDownLatch;  // locks thread while waiting for binding
        private static final long bindingTimeoutSeconds = 5;
        private CountDownLatch responseCountDownLatch;  // locks thread while waiting for service response
        private static final long responseTimeoutSeconds = 5;
        private boolean bound = false;

        private Messenger npmServiceOutMessenger;  // sends message to service to request stored permission request data
        private Messenger npmServiceInMessenger;  // receives response


        RequestHandlerThread(Object uiObject, int requestCode, String[] requestingPermissions, Context applicationContext) {
            this.uiObject = uiObject;
            this.requestCode = requestCode;
            this.requestingPermissions = requestingPermissions;
            this.applicationContext = applicationContext;
            this.packageName = applicationContext.getPackageName();

            handlerThread = new HandlerThread("XRequestPermissionsMessengerHandlerThread");
            handlerThread.start();
            npmServiceInMessenger = new Messenger(new IncomingHandler(handlerThread.getLooper()));
            mConnection = new MyServiceConnection();
        }

        @Override
        public void run() {
            int[] grantResults;
            try {
                // MyLogHelper.logDebug(TAG, "Fetching grant results");
                grantResults = fetchGrantResult(requestingPermissions);
            } catch (InterruptedException e) {
                XposedLogger.logError(TAG, packageName, "Latch interrupted while fetching grant result from service.");
                grantResults = null;
                XposedLogger.logException(e);
            }

            if (grantResults == null) {
                // if something fails, we deny everything to force re-asking. todo is this the best approach?
                XposedLogger.logDebug(TAG, packageName, "null grantResults. Returning deny for all requests.");
                grantResults = new int[requestingPermissions.length];
                Arrays.fill(grantResults, PackageManager.PERMISSION_DENIED);
            }

            XposedLogger.logDebug(TAG, packageName, "Got grantResults = " + Arrays.toString(grantResults) + " (empty means cancelling).");
            final int[] finalGrantResults = grantResults;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Method onRequestPermissionsResultMethod = XposedHelpers.findMethodExactIfExists(
                            uiObject.getClass().getName(), uiObject.getClass().getClassLoader(),
                            "onRequestPermissionsResult", "int", String[].class, "int[]");
                    if (onRequestPermissionsResultMethod == null) {
                        XposedLogger.logError(TAG, packageName, "Failed to obtain onRequestPermissionsResult method from uiObject");
                        return;
                    }

                    try {
                        XposedLogger.logDebug(TAG, packageName, "Invoking onRequestPermissionsResult with grant results = " + Arrays.toString(finalGrantResults));
                        onRequestPermissionsResultMethod.invoke(uiObject, requestCode, requestingPermissions, finalGrantResults);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        // this won't happen unless I'm missing something or the Android api changes...
                        XposedLogger.logError(TAG, packageName, "Failed to invoke onRequestPermissionsResult: " + e);
                        XposedLogger.logException(e);
                        // todo: the app will possibly wait forever for this call. How to handle?
                    }
                }
            });
        }

        @Nullable
        private int[] fetchGrantResult(String[] requestingPermissions) throws InterruptedException {
            int[] grantResults = new int[requestingPermissions.length];
            XposedLogger.logDebug(TAG, packageName, "Fetching grant results. requestingPermissions=" + Arrays.toString(requestingPermissions));
            for (int i=0; i<requestingPermissions.length; i++) {
                String permission = requestingPermissions[i];
                String permissionGroup = XposedHelper.getPermissionGroup(applicationContext, permission);
                XCachedPermissionResult cachedResult = XCheckPermissionHookHandler.getCachedResult(permission, permissionGroup);
                if (cachedResult != null && cachedResult.stillValid()) {
                    XposedLogger.logDebug(TAG, packageName, "Permission in cache: " + cachedResult);
                    grantResults[i] = cachedResult.grantResult;
                } else {
                    // if the permission is not in cache it was probably not requested yet. This should be very rare given that apps should check before requesting
                    XposedLogger.logDebug(TAG, packageName, "Permission " + permission + " is not in cache. Retrieving from service");
                    if (cachedResult != null)
                        XCheckPermissionHookHandler.removeCachedResult(permission, permissionGroup);  // remove invalid result from cache if there's any

                    applicationContext.checkSelfPermission(permission);  // we force the XCheckPermissionHook
                    cachedResult = XCheckPermissionHookHandler.getCachedResult(permission, permissionGroup);
                    if (cachedResult != null && cachedResult.stillValid()) {
                        XposedLogger.logDebug(TAG, packageName, "Got permission result after forcing permission check: " + cachedResult);
                        grantResults[i] = cachedResult.grantResult;
                    } else {
                        // this should never happen
                        XposedLogger.logError(TAG, packageName, "Failed to retrieve permission result after forcing permission check.");
                        return null;
                    }

//                    // fetch from service. If service doesn't have, either return allow or deny, so that things don't break
//                    if (!bound) {
//                        Intent bindServiceIntent = new Intent();
//                        // because the service is in other app
//                        bindServiceIntent.setComponent(new ComponentName(BuildConfig.APPLICATION_ID, BuildConfig.APPLICATION_ID + ".services.PermissionManagerService"));
//                        applicationContext.bindService(bindServiceIntent, mConnection, Context.BIND_AUTO_CREATE);
//                        bindingCountDownLatch = new CountDownLatch(1);
//                        bindingCountDownLatch.await(bindingTimeoutSeconds, TimeUnit.SECONDS);  // wait while binding
//                    }
//                    if (!bound) {  // can be either interrupted or timedout
//                        XposedLogger.logDebug(TAG, packageName, "Failed to bind service in the set up time. No results fetched.");
//                        return null;
//                    }
//
//                    // MyLogHelper.logDebug(TAG, "Service is bound. Requesting package valid permissions");
//                    Bundle toSendBundle = new Bundle();
//                    toSendBundle.putString(PermissionManagerService.KEY_PCKG_NAME, applicationContext.getPackageName());
//                    Message msg = Message.obtain(null, PermissionManagerService.MSG_GET_PCKG_VALID_PERMISSIONS, toSendBundle);
//                    msg.replyTo = npmServiceInMessenger;
//                    try {
//                        npmServiceOutMessenger.send(msg);
//                    } catch (RemoteException e) {
//                        // If we reach here, means that the connection to the service is broken, so we force disconnect
//                        mConnection.onServiceDisconnected(null);
//                        return null;
//                    }
//
//                    // MyLogHelper.logDebug(TAG, "Awaiting service response");
//                    responseCountDownLatch = new CountDownLatch(1);
//                    responseCountDownLatch.await(responseTimeoutSeconds, TimeUnit.SECONDS);
//                    // if all went well, the cache should now be updated.
//                    cachedResult = XCheckPermissionHook.getCachedResult(permission, permissionGroup);
//                    if (cachedResult != null) {
//                        grantResults[i] = cachedResult.grantResult;
//                    } else {
//                        // this can happen if our permission request was dismissed or there was some error with our check permission hook
//                        XposedLogger.logWarn(TAG, packageName, "Found no permission result in database for permission " + permission);
//                        return null;
//                        // this should never happen because our check permission hooks should have stored it.
//                        // it is possible however that badly programmed apps check for 1 permission while requesting multiple.
//                    }
                }
            }
            return grantResults;
        }

        private class MyServiceConnection implements ServiceConnection {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                XposedLogger.logDebug(TAG, packageName, "Service bound. Was bound?" + bound);
                npmServiceOutMessenger = new Messenger(service);
                bound = true;
                bindingCountDownLatch.countDown();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                XposedLogger.logDebug(TAG, packageName, "Service Disconnected");
                npmServiceOutMessenger = null;
                bound = false;
                bindingCountDownLatch.countDown();
            }
        }

        private class IncomingHandler extends Handler {
            IncomingHandler(Looper looper) {
                super(looper);
            }

            @Override
            public void handleMessage(Message msg) {
                if (msg.what == PermissionManagerService.MSG_GET_PCKG_VALID_PERMISSIONS) {
                    XposedLogger.logDebug(TAG, packageName, "Received package valid permissions from service");

                    Bundle data = (Bundle) msg.obj;
                    ArrayList<Bundle> permissionDataBundleArray = data.getParcelableArrayList(PermissionManagerService.KEY_PCKG_VALID_PERMISSIONS);
                    if (permissionDataBundleArray == null) {
                        // it can be empty but not null
                        XposedLogger.logError(TAG, packageName, "Received null data from service. Failed to handle message due to possible bug in the service.");
                    } else {
                        for (Bundle bundle: permissionDataBundleArray) {
                            PermissionDialogData permissionData = PermissionDialogData.fromBundle(bundle);
                            XCheckPermissionHookHandler.addCachedResult(permissionData.checkedPermission, permissionData.checkedPermissionGroup, new XCachedPermissionResult(permissionData));
                        }
                    }
                    responseCountDownLatch.countDown();

                    // clean up
                    applicationContext.unbindService(mConnection);
                    handlerThread.quitSafely();
                } else {
                    // ignore message
                    XposedLogger.logWarn(TAG, packageName, "Receive wrong expected message from service. Bug?");
                }
            }
        }
    }

    private Activity getThisObjectActivity(Object o) {
        // todo test this hook when requesting permission from fragment
        // We return the activity because we can always cast it to context, but getting the context here could also work
        Class cls = o.getClass();
        ClassLoader clsLoader = cls.getClassLoader();

        // from the methods in XPermissions.java
        String[] possibleClassArray = {"android.app.Activity", "android.app.Fragment", "androidx.fragment.app.Fragment", "android.support.v4.app.Fragment"};
        for (String possibleClass: possibleClassArray) {
            Class<?> possibleClazz = XposedHelpers.findClassIfExists(possibleClass, clsLoader);
            if (possibleClazz != null && possibleClazz.isInstance(o)) {
                // MyLogHelper.logDebug(TAG, "Object is instance of " + possibleClass);
                if (possibleClass.equals("android.app.Activity")) {
                    return (Activity) o;
                } else {
                    // A fragment might be associated either with a context or an activity
                    // Object fragmentObject = possibleClazz.cast(o);
                    Method getActivity = XposedHelpers.findMethodExactIfExists(possibleClazz, "getActivity");
                    if (getActivity == null) {
                        XposedLogger.logError(null, TAG, "Failed to get activity from fragment.");
                        return null;
                    }
                    try {
                        return (Activity) getActivity.invoke(o);  // can be null here
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        XposedLogger.logError(null, TAG, "Failed to invoke getActivity: " + e);
                        return null;
                    }
                }
            }
        }
        return null;  // failed to find class
    }
}
