package pt.uc.dei.copmode.npm.backend.converters;

import androidx.room.TypeConverter;

import pt.uc.dei.copmode.npm.backend.entities.NetworkStatus;

public class NetworkStatusConverter {
    @TypeConverter
    public static NetworkStatus toNetworkStatus(int status) {
        if (status == NetworkStatus.DISCONNECTED.getStatus()) {
            return NetworkStatus.DISCONNECTED;
        } else if (status == NetworkStatus.NOT_METERED.getStatus()) {
            return NetworkStatus.NOT_METERED;
        } else if (status == NetworkStatus.METERED.getStatus()) {
            return NetworkStatus.METERED;
        } else {
            throw new IllegalArgumentException("Could not recognize network status");
        }
    }

    @TypeConverter
    public static Integer toInteger(NetworkStatus networkStatus) {
        return networkStatus.getStatus();
    }
}
