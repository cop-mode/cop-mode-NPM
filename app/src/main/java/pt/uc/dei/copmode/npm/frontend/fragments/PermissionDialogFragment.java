package pt.uc.dei.copmode.npm.frontend.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import pt.uc.dei.copmode.npm.R;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

import static androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY;

public class PermissionDialogFragment extends DialogFragment {
    private final static String TAG = "PermissionDF";

    private PermissionDialogData permissionDialogData;
    private PermissionDialogListener listener;

    public static final long TIMEOUT_SECONDS = 40L;

    private static final int UNANSWERED = 3;
    private static final int YES = 2;
    private static final int NO = 1;
    private static final int DONT_KNOW = 0;

    // dialog results
    private String selectedSemanticLoc = null;
    private int wasRequestExpected = UNANSWERED;
    private int grantResult = UNANSWERED;  // 0 is granted and -1 is denied

    public PermissionDialogFragment(PermissionDialogData permissionDialogData) {
        super();
        this.permissionDialogData = permissionDialogData;
    }

    public interface PermissionDialogListener {
        void onDialogConfirmClick(PermissionDialogData permissionDialogData);
        void onDialogTimeout(PermissionDialogData permissionDialogData);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (PermissionDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement PermissionDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View dialogView = generateDialogView();
        setCancelable(false);

        return new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .create();
    }

    @NonNull
    private View generateDialogView() {
        Activity activity = requireActivity();
        LayoutInflater inflater = activity.getLayoutInflater();
        PackageManager pm = activity.getPackageManager();

        View dialogView = inflater.inflate(R.layout.dialog_permission_request, null);

        setupAppInfoViews(pm, dialogView);

        setupPermissionRequestsList(pm, dialogView, activity);

        setupSemanticLocations(dialogView);

        setupExpectedPermissionRequest(dialogView);

        setupConfirmAndValidation(activity, dialogView);

        setupTimeout(dialogView);

        return dialogView;
    }

    private void setupAppInfoViews(PackageManager pm, View dialogView) {
        try {
            ApplicationInfo requestingAppInfo = pm.getApplicationInfo(permissionDialogData.requestingApplicationInfo.packageName, 0);
            ImageView ivAppIcon = dialogView.findViewById(R.id.app_icon);
            ivAppIcon.setImageDrawable(pm.getApplicationIcon(requestingAppInfo));
        } catch (PackageManager.NameNotFoundException e) {
            MyLogHelper.logWarn(TAG, "Failed to get the requesting app icon");
        }
        TextView tvAppName = dialogView.findViewById(R.id.app_name);
        tvAppName.setText(permissionDialogData.requestingApplicationInfo.name);
    }

    private void setupPermissionRequestsList(final PackageManager pm, View dialogView, Activity activity) {
        TextView tvPermissionDescription = dialogView.findViewById(R.id.permission_description);

        try {  //
            String permissionGroup = pm.getPermissionInfo(permissionDialogData.checkedPermission, 0).group;
            // tvPermissionDescription.setText(pm.getPermissionGroupInfo(permissionGroup, 0).loadLabel(pm)); // localized
            IconAndPermissionDescription iconAndPermissionDescription = getPermissionDescriptionFromGroup(permissionGroup, activity);
            tvPermissionDescription.setText(iconAndPermissionDescription.permissionDescription);
            ImageView ivPermissionIcon = dialogView.findViewById(R.id.permission_icon);
            ivPermissionIcon.setImageDrawable(iconAndPermissionDescription.icon);
        } catch (PackageManager.NameNotFoundException e) {
            // this should never happen. If it does we set the name to the permission itself
            // listener.onDialogError(this, requestCode, "Permission name not found: " + e);
            String[] split = permissionDialogData.checkedPermission.split("\\.");
            tvPermissionDescription.setText(split[split.length-1]);
        }

        final ToggleButton denyToggle = dialogView.findViewById(R.id.toggle_deny);
        final ToggleButton allowToggle = dialogView.findViewById(R.id.toggle_allow);
        denyToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    allowToggle.setChecked(false);
                    grantResult = PackageManager.PERMISSION_DENIED;
                } else {
                    grantResult = UNANSWERED;
                }
            }
        });
        allowToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    denyToggle.setChecked(false);
                    grantResult = PackageManager.PERMISSION_GRANTED;
                } else {
                    grantResult = UNANSWERED;
                }
            }
        });
    }

    private IconAndPermissionDescription getPermissionDescriptionFromGroup(String permissionGroup, Activity activity) {
        String appName = permissionDialogData.requestingApplicationInfo.name;
        IconAndPermissionDescription iconAndPermissionDescription = new IconAndPermissionDescription();
        switch (permissionGroup) {
            case Manifest.permission_group.CONTACTS:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_contacts, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_contacts_permission_24);
                break;
            case Manifest.permission_group.LOCATION:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_location, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_location_permission_24);
                break;
            case Manifest.permission_group.CALENDAR:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_calendar, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_calendar_permission_24);
                break;
            case Manifest.permission_group.SMS:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_sms, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_sms_permission_24);
                break;
            case Manifest.permission_group.STORAGE:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_storage, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_storage_permission_24);
                break;
            case Manifest.permission_group.MICROPHONE:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_microphone, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_microphone_permission_24);
                break;
            case Manifest.permission_group.ACTIVITY_RECOGNITION:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_activityRecognition, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_activity_recognition_permission_24);
                break;
            case Manifest.permission_group.CAMERA:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_camera, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_camera_permission_24);
                break;
            case Manifest.permission_group.CALL_LOG:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_calllog, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_call_log_permission_24);
                break;
            case Manifest.permission_group.PHONE:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_phone, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_phone_permission_24);
                break;
            case Manifest.permission_group.SENSORS:
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(getString(R.string.permgrouprequest_sensors, appName), FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_sensors_permission_24);
                break;
            default:
                // this happens only if the API changes
                iconAndPermissionDescription.permissionDescription = Html.fromHtml(permissionGroup, FROM_HTML_MODE_LEGACY);
                iconAndPermissionDescription.icon = ContextCompat.getDrawable(activity, R.drawable.ic_other_permission_24);
        }
        return iconAndPermissionDescription;
    }

    private void setupSemanticLocations(View dialogView) {
        ToggleButton homeToggle = dialogView.findViewById(R.id.home_toggle);
        ToggleButton workToggle = dialogView.findViewById(R.id.work_toggle);
        ToggleButton travellingToggle = dialogView.findViewById(R.id.travelling_toggle);
        ToggleButton otherToggle = dialogView.findViewById(R.id.other_toggle);

        final ToggleButton[] toggles = {homeToggle, workToggle, travellingToggle, otherToggle};
        CompoundButton.OnCheckedChangeListener togglesHandler = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                for (ToggleButton toggle : toggles) {
                    toggle.setChecked(false);
                    selectedSemanticLoc = null;
                }
                buttonView.setChecked(isChecked);
                if (isChecked)
                    selectedSemanticLoc = (String) buttonView.getTag();
            }
        };

        for (ToggleButton toggle : toggles) {
            toggle.setOnCheckedChangeListener(togglesHandler);
        }
    }

    private void setupExpectedPermissionRequest(View dialogView) {
        final ToggleButton toggleNotExpectedPrompt = dialogView.findViewById(R.id.toggle_not_expected_prompt);
        toggleNotExpectedPrompt.setTag(NO);
        final ToggleButton toggleExpectedPrompt = dialogView.findViewById(R.id.toggle_expected_prompt);
        toggleExpectedPrompt.setTag(YES);
        final ToggleButton toggleDontKnowExpectedPrompt = dialogView.findViewById(R.id.toggle_dont_know_expected_prompt);
        toggleDontKnowExpectedPrompt.setTag(DONT_KNOW);

        final ToggleButton[] expectedPromptToggles = {toggleNotExpectedPrompt, toggleExpectedPrompt, toggleDontKnowExpectedPrompt};

        CompoundButton.OnCheckedChangeListener togglesHandler = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                for (ToggleButton toggle : expectedPromptToggles) {
                    toggle.setChecked(false);
                    wasRequestExpected = UNANSWERED;
                }
                buttonView.setChecked(isChecked);
                if (isChecked)
                    wasRequestExpected = (int) buttonView.getTag();
            }
        };

        for (ToggleButton button: expectedPromptToggles) {
            button.setOnCheckedChangeListener(togglesHandler);
        }
    }

    private void setupConfirmAndValidation(final Activity activity, final View dialogView) {
        Button button = dialogView.findViewById(R.id.confirm_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String error = validateAnswers(grantResult, selectedSemanticLoc, wasRequestExpected);
                if (error != null) {
                    Toast.makeText(activity, error, Toast.LENGTH_LONG).show();
                } else {
                    permissionDialogData.grantResult = grantResult;
                    permissionDialogData.selectedSemanticLoc = selectedSemanticLoc;
                    permissionDialogData.wasRequestExpected = wasRequestExpected;
                    listener.onDialogConfirmClick(permissionDialogData);
                }
            }
        });
    }

    private String validateAnswers(int grantResult, String semanticLocation, int wasRequestExpected) {
        if (grantResult == UNANSWERED)
            return getString(R.string.unanswered_permission_requests_message);
        if (semanticLocation == null)
            return getString(R.string.unanswered_semantic_location_message);
        if (wasRequestExpected == UNANSWERED)
            return getString(R.string.unanswered_was_request_expected_message);
        return null;
    }

    private void setupTimeout(View dialogView) {
        final CircularProgressBar circularProgressBar = dialogView.findViewById(R.id.timeout_progress_indicator);
        circularProgressBar.setProgressMax((float) TIMEOUT_SECONDS);
        // circularProgressBar.setProgressWithAnimation((float) TIMEOUT_SECONDS, 1000L); // =1s
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                float currentProgress = circularProgressBar.getProgress();
                if (currentProgress > 0) {
                    circularProgressBar.setProgress(currentProgress - 1);  // loses 1 each second
                    if (PermissionDialogFragment.this.isAdded())  // if the fragment is gone, no need to keep counting
                        handler.postDelayed(this, 1000L);
                } else {
                    listener.onDialogTimeout(permissionDialogData);
                    if (PermissionDialogFragment.this.isAdded())  // for safety, as the user might have dismissed in the last second
                        dismissAllowingStateLoss();
                }
            }
        });
    }

    private static class IconAndPermissionDescription {
        Spanned permissionDescription;
        Drawable icon;
    }
}