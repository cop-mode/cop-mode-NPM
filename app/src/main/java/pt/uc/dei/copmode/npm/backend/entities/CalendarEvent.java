package pt.uc.dei.copmode.npm.backend.entities;

import android.provider.CalendarContract;

import java.util.HashMap;
import java.util.Map;

public class CalendarEvent {
    public long instanceId;
    public long calendarId;
    public long eventId;
    public long eventBegin;
    public long eventEnd;
    public String eventLocation;
    public int eventStatus;

    public CalendarEvent(long instanceId, long calendarId, long eventId, long eventBegin, long eventEnd, String eventLocation, int eventStatus) {
        this.instanceId = instanceId;
        this.calendarId = calendarId;
        this.eventId = eventId;
        this.eventBegin = eventBegin;
        this.eventEnd = eventEnd;
        this.eventLocation = eventLocation;
        this.eventStatus = eventStatus;
    }


}
