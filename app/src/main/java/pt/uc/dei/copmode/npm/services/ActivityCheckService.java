package pt.uc.dei.copmode.npm.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Handler;


import androidx.core.app.NotificationManagerCompat;

import java.util.Date;

import pt.uc.dei.copmode.npm.backend.database.NPMDatabase;
import pt.uc.dei.copmode.npm.backend.database.PermissionDialogDataDao;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;
import pt.uc.dei.copmode.npm.helpers.NotificationHelper;

/**
 * This service checks if the user has been active recently. If it wasn't, it issues a notification
 * to alert the "inactiveness".
 */
@SuppressLint("SpecifyJobSchedulerIdRange")
public class ActivityCheckService extends JobService {
    private static final String TAG = "ActivityCheckService";

    // Inactivity is measured as not answering at least minimumPermissionAnswers in ACTIVITY_WINDOW_MILLIS time.
    private static final long ACTIVITY_WINDOW_MILLIS = 24 * 3600000L;
    private static final int minimumPermissionAnswers = 5;

    @Override
    public boolean onStartJob(JobParameters params) {
        MyLogHelper.logDebug(TAG, TAG + " running");

        // Run on independent thread
        new Thread() {
            @Override
            public void run() {
                NPMDatabase db = NPMDatabase.getDB(getApplicationContext());
                PermissionDialogDataDao dao = db.permissionDialogDataDao();

                long minTimestamp = System.currentTimeMillis() - ACTIVITY_WINDOW_MILLIS;

                PermissionDialogData firstUserAnsweredPermissionDialogData = dao.getFirstPermissionDialogDataWithAnswerType(PermissionDialogAnswerType.USER_ANSWERED);
                if(firstUserAnsweredPermissionDialogData != null) {
                    // The participant has started the campaign
                    // MyLogHelper.logDebug(TAG, "First user Answered Permission was in " + new Date(firstUserAnsweredPermissionDialogData.timestamp));
                    if(minTimestamp > firstUserAnsweredPermissionDialogData.timestamp) {
                        // The participant has started the campaign at least ACTIVITY_WINDOW_MILLIS time ago
                        PermissionDialogData[] retrievedData = dao.getAllPermissionDialogDataWithAnswerTypeSince(PermissionDialogAnswerType.USER_ANSWERED, minTimestamp);
                        // MyLogHelper.logDebug(TAG, "" + retrievedData.length);
                        if(retrievedData.length < minimumPermissionAnswers) {
                            Notification warningNotification = NotificationHelper.createActivityWarningNotification(getApplicationContext());
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
                            notificationManager.notify(NotificationHelper.ACTIVITY_WARNING_NOTIFICATION_ID, warningNotification);
                        }
                    }
                }
            }
        }.start();

        // always reschedule
        jobFinished(params, true);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }
}
