package pt.uc.dei.copmode.npm.frontend.adapters;

import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import pt.uc.dei.copmode.npm.R;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.helpers.DateHelper;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;


public class PermissionRequestsViewAdapter extends ListAdapter<PermissionDialogData, PermissionRequestsViewAdapter.MyViewHolder> {
    private final static String TAG = "PermissionRequestsViewAdapter";
    public PermissionRequestsViewAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.permission_request_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        PermissionDialogData itemData = getItem(position);
        TextView tv = holder.itemView.findViewById(R.id.date_text_view);
        tv.setText(DateHelper.getFormattedDate(itemData.timestamp, "HH:mm:ss\nyyyy.MM.dd"));

        ImageView iconView = holder.itemView.findViewById(R.id.app_icon_image_view);
        PackageManager pm = holder.itemView.getContext().getPackageManager();
        try {  // sucks to have logic here... The alternative is to have a viewmodel and make a model for this itemview. The dataset would then be an array of this model
            iconView.setImageDrawable(pm.getApplicationIcon(itemData.requestingApplicationInfo.packageName));
        } catch (PackageManager.NameNotFoundException ignored) { }

        tv = holder.itemView.findViewById(R.id.app_name_text_view);
        tv.setText(itemData.requestingApplicationInfo.name);

        tv = holder.itemView.findViewById(R.id.requested_permission_text_view);
        try {
            String permissionGroup = pm.getPermissionInfo(itemData.checkedPermission, 0).group;
            if (permissionGroup == null) {
                tv.setText(itemData.checkedPermission);
            } else {
                tv.setText(pm.getPermissionGroupInfo(permissionGroup, 0).loadLabel(pm)); // localized
            }
        } catch (PackageManager.NameNotFoundException e) {
            MyLogHelper.logException(e);
        }

        iconView = holder.itemView.findViewById(R.id.decision_icon_view);
        if (itemData.wasGranted()) {
            iconView.setImageResource(R.drawable.ic_decision_grant_36dp);

        } else {
            iconView.setImageResource(R.drawable.ic_decision_deny_36dp);
        }

        tv = holder.itemView.findViewById(R.id.decision_type_text_view);
        if (itemData.answerType == PermissionDialogAnswerType.USER_ANSWERED) {
            tv.setText("U");
        } else if (itemData.answerType == PermissionDialogAnswerType.CACHE_ANSWERED) {
            tv.setText("C");
        } else if (itemData.answerType == PermissionDialogAnswerType.TIMEDOUT) {
            tv.setText("T");
        } else if (itemData.answerType == PermissionDialogAnswerType.DISMISSED) {
            tv.setText("D");
        } else {
            // this shouldn't happen. If it does, we should know
            tv.setText("?");
            MyLogHelper.logError(TAG, "Trying to display an incomplete permission dialog data. This shouldn't be in DB: " + itemData);
        }
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        MyViewHolder(View v) {
            super(v);
        }
    }

    private static final DiffUtil.ItemCallback<PermissionDialogData> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<PermissionDialogData>() {
                @Override
                public boolean areItemsTheSame(@NonNull PermissionDialogData oldItem, @NonNull PermissionDialogData newItem) {
                    return oldItem.getPermissionDialogDataId() == newItem.getPermissionDialogDataId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull PermissionDialogData oldItem, @NonNull PermissionDialogData newItem) {
                    return oldItem.equals(newItem);
                }
            };
}
