package pt.uc.dei.copmode.npm.xposed;

import android.content.Context;
import android.content.pm.PackageManager;

public class XposedHelper {
    public static String getPermissionGroup(Context context, String checkedPermission) {
        PackageManager pm = context.getPackageManager();
        try {
            return pm.getPermissionInfo(checkedPermission, 0).group;
        } catch (PackageManager.NameNotFoundException e) {
            XposedLogger.logException(e);
            return null;
        }
    }
}
