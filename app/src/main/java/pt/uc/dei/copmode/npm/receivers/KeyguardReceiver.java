package pt.uc.dei.copmode.npm.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import pt.uc.dei.copmode.npm.components.ContextualDataHolder;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

public class KeyguardReceiver extends BroadcastReceiver implements ContextReceiver {
    private final static String TAG = "KeyguardReceiver";
    private boolean isReceiving = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null)
            return;
        if (action.equals(Intent.ACTION_USER_PRESENT)) {
            MyLogHelper.logDebug(TAG, "Keyguard unlocked");
            ContextualDataHolder.getInstance(context).updateIsKeyguardLocked(false);
            // keyguard is locked again when screen goes off
        }
    }

    @Override
    public boolean registerSelf(Context context) {
        IntentFilter filter = new IntentFilter(Intent.ACTION_USER_PRESENT);
        context.registerReceiver(this, filter);
        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }
}
