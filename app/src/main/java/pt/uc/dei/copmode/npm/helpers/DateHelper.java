package pt.uc.dei.copmode.npm.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateHelper {
    public static final String DEFAULT_DATE_PATTERN = "HH:mm:ss yyyy.MM.dd";

    public static String getFormattedDate(long timestamp, String pattern) {

        try{
            Date date = new Date(timestamp);
            SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.getDefault());
            return formatter.format(date);
            // return DateFormat.getDateInstance().format(date);
        }
        catch(Exception ex){
            return Long.toString(timestamp);
        }
    }
}
