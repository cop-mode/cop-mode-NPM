package pt.uc.dei.copmode.npm.helpers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import pt.uc.dei.copmode.npm.R;
import pt.uc.dei.copmode.npm.frontend.activities.MainActivity;
import pt.uc.dei.copmode.npm.frontend.activities.StatisticsActivity;

public class NotificationHelper {
    private static String TAG = "NotificationHelper";

    private static final String RUNNING_STATUS_CHANNEL_ID = "pt.uc.dei.copmode.npm.permissionmanagerservice.notification.RUNNING_STATUS_CHANNEL_ID";
    public static final int RUNNING_STATUS_NOTIFICATION_ID = 1;

    private static final String ACTIVITY_WARNING_CHANNEL_ID = "pt.uc.dei.copmode.npm.permissionmanagerservice.notification.ACTIVITY_WARNING_CHANNEL_ID";
    public static final int ACTIVITY_WARNING_NOTIFICATION_ID = 2;

    public static Notification createRunningStatusNotification(Context context) {
        String channelName = context.getString(R.string.running_status_notification_channel_name);
        String channelDescription = context.getString(R.string.running_status_notification_channel_description);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationCompat.Builder builder = getNotificationBuilder(context, channelName, channelDescription, RUNNING_STATUS_CHANNEL_ID, importance);
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        return builder.setOngoing(true)
                .setContentTitle(context.getText(R.string.running_status_notification_content_title))
                .setContentText(context.getText(R.string.running_status_notification_content_text))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .build();
    }

    public static Notification createActivityWarningNotification(Context context) {
        String channelName = context.getString(R.string.user_activity_notification_channel_name);
        String channelDescription = context.getString(R.string.user_activity_notification_channel_description);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationCompat.Builder builder = getNotificationBuilder(context, channelName, channelDescription, ACTIVITY_WARNING_CHANNEL_ID, importance);
        Intent intent = new Intent(context, StatisticsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        return builder.setOngoing(false)
                .setContentTitle(context.getText(R.string.user_activity_notification_content_title))
                .setContentText(context.getText(R.string.user_activity_notification_content_text))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(context.getString(R.string.user_activity_notification_content_big_text)))
                .setSmallIcon(R.drawable.ic_baseline_warning_24)
                .setContentIntent(pendingIntent)
                .build();
    }

    private static NotificationCompat.Builder getNotificationBuilder(Context context, String channelName, String channelDescription, String channelId, int importance) {
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            prepareNotificationChannel(context, channelName, channelDescription, channelId, importance);
        }
        builder = new NotificationCompat.Builder(context, channelId);
        return builder;
    }

    @TargetApi(26)
    private static void prepareNotificationChannel(Context context, String channelName, String channelDescription, String channelId, int importance) {

        final NotificationManager nm = (NotificationManager) context.getSystemService(Activity.NOTIFICATION_SERVICE);
        if (nm != null) {
            NotificationChannel nChannel = nm.getNotificationChannel(channelId);
            if (nChannel == null) {
                nChannel = new NotificationChannel(channelId, channelName, importance);
                nChannel.setDescription(channelDescription);
                nm.createNotificationChannel(nChannel);
            }
        } else {
            MyLogHelper.logError(TAG, "Failed to get Notitification Manager when preparing channel with " + channelName);
        }
    }
}
