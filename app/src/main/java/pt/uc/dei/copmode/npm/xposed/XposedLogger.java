package pt.uc.dei.copmode.npm.xposed;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

/**
 * Logger to used in xposed modules.
 * Note that we cannot use Firebase here because the app might not have the dependency.
 */
public class XposedLogger {
    public static void logInfo(String tag, @Nullable String packageName, String msg) {
        String packageNamePrefix = packageName != null ? "[" + packageName + "] " : "";
        Log.i(tag, packageNamePrefix + msg);
    }

    public static void logDebug(String tag, @Nullable String packageName, String msg) {
        String packageNamePrefix = packageName != null ? "[" + packageName + "] " : "";
        Log.d(tag, packageNamePrefix + msg);
    }

    public static void logWarn(String tag, @Nullable String packageName, String msg) {
        String packageNamePrefix = packageName != null ? "[" + packageName + "] " : "";
        Log.w(tag, packageNamePrefix + msg);
    }

    public static void logError(String tag, @Nullable String packageName, String msg) {
        String packageNamePrefix = packageName != null ? "[" + packageName + "] " : "";
        Log.e(tag, packageNamePrefix + msg);
    }

    public static void logException(Exception e) {
        e.printStackTrace();
    }
}
