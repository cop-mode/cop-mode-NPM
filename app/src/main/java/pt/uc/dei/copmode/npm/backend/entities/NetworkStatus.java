package pt.uc.dei.copmode.npm.backend.entities;

public enum NetworkStatus {
    DISCONNECTED(0),
    NOT_METERED(1),
    METERED(2);

    private int status = 0;
    public int getStatus() {
        return status;
    }

    // for room
    NetworkStatus(int status) {
        this.status = status;
    }
}
