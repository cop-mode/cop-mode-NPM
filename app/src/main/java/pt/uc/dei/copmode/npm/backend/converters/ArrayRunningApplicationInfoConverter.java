package pt.uc.dei.copmode.npm.backend.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import pt.uc.dei.copmode.npm.backend.entities.CalendarEvent;
import pt.uc.dei.copmode.npm.backend.entities.RunningApplicationInfo;

public class ArrayRunningApplicationInfoConverter {
    @TypeConverter
    public static RunningApplicationInfo[] toArray(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<RunningApplicationInfo[]>() {}.getType();
        return gson.fromJson(json, type);
    }

    @TypeConverter
    public static String fromArray(RunningApplicationInfo[] array) {
        Gson gson = new Gson();
        return gson.toJson(array);
    }
}
