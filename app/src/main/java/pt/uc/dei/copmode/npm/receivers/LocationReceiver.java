package pt.uc.dei.copmode.npm.receivers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import pt.uc.dei.copmode.npm.components.ContextualDataHolder;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

public class LocationReceiver implements ContextReceiver {
    private static final String TAG = "LocationReceiver";
    private boolean isReceiving = false;

    private static Context mContext;
    private static Location currentLocation;

    private static final long REQUESTS_MIN_INTERVAL_MILLIS = 60000; // once a minute
    private static final float REQUESTS_MIN_DISTANCE_METERS = 30;
    private static final long MAX_VALIDATY_NANOS = (long) (120 * Math.pow(10, 9));  // 2 minutes

    @Override
    public boolean registerSelf(Context context) {
        mContext = context;

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            MyLogHelper.logDebug(TAG, "Failed to start " + TAG + " due to missing location permissions");
            isReceiving = false;
            return false;
        }

        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, REQUESTS_MIN_INTERVAL_MILLIS, REQUESTS_MIN_DISTANCE_METERS, new MyLocationListener());
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, REQUESTS_MIN_INTERVAL_MILLIS, REQUESTS_MIN_DISTANCE_METERS, new MyLocationListener());
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, REQUESTS_MIN_INTERVAL_MILLIS, REQUESTS_MIN_DISTANCE_METERS, new MyLocationListener());

        // initialize location prioritizing GPS.
        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (currentLocation == null)
            currentLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        if (currentLocation == null)
            currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (currentLocation != null)
            ContextualDataHolder.getInstance(context).updateLocation(currentLocation);
        MyLogHelper.logDebug(TAG, "Started Location receiver. Current location: " + currentLocation);
        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }

    private static Location pickBestLocation(Location loc1, Location loc2) {
        if (loc1 == null) {
            if (loc2 != null)
                return loc2;
            else  // both null
                return null;
        } else {
            if (loc2 == null)
                return loc1;
        }

        long currentElapsedRealtimeNanos = SystemClock.elapsedRealtime();
        Location mostAccurateLocation = (loc1.getAccuracy() <= loc2.getAccuracy()) ? loc1 : loc2;  // less is better
        boolean mostAccurateLocationIsExpired = (currentElapsedRealtimeNanos - mostAccurateLocation.getElapsedRealtimeNanos() >  MAX_VALIDATY_NANOS);
        Location mostRecentLocation = (loc1.getElapsedRealtimeNanos() <= loc2.getElapsedRealtimeNanos()) ? loc1 : loc2;
        boolean mostRecentLocationIsExpired = (currentElapsedRealtimeNanos - mostRecentLocation.getElapsedRealtimeNanos() >  MAX_VALIDATY_NANOS);

        if (mostAccurateLocation == mostRecentLocation || !mostAccurateLocationIsExpired) {
            return mostAccurateLocation;
        } else {
            // most accurate is not the most recent and is expired
            if (mostRecentLocationIsExpired) {
                // both are expired
                return mostAccurateLocation;
            } else {
                return mostRecentLocation;
            }
        }
    }


    private static class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location newLocation) {
            if (newLocation == null)
                return;

            MyLogHelper.logDebug(TAG, "Got new location: " + newLocation);
            currentLocation = pickBestLocation(currentLocation, newLocation);
            if (currentLocation == newLocation)
                ContextualDataHolder.getInstance(mContext).updateLocation(currentLocation);
        }

        @Override
        @Deprecated
        public void onStatusChanged(String provider, int status, Bundle extras) { }

        @Override
        public void onProviderEnabled(String provider) {
            MyLogHelper.logDebug(TAG, "Location provider " + provider + " enabled.");
        }

        @Override
        public void onProviderDisabled(String provider) {
            MyLogHelper.logDebug(TAG, "Location provider " + provider + " disabled.");
        }
    }


}
