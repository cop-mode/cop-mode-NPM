package pt.uc.dei.copmode.npm.backend.entities;

import android.location.Location;

import androidx.annotation.NonNull;

public class LocationData {
    public double latitude;
    public double longitude;
    public float horizontalAccuracyMeters;
    public long elapsedRealtimeNanos;
    public long time;

    public LocationData(double latitude, double longitude, float horizontalAccuracyMeters, long elapsedRealtimeNanos, long time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.horizontalAccuracyMeters = horizontalAccuracyMeters;
        this.elapsedRealtimeNanos = elapsedRealtimeNanos;
        this.time = time;
    }

    public LocationData(@NonNull Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        horizontalAccuracyMeters = location.getAccuracy();
        elapsedRealtimeNanos = location.getElapsedRealtimeNanos();
        time = location.getTime();
    }
}
