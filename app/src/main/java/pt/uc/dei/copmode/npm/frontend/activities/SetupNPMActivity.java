package pt.uc.dei.copmode.npm.frontend.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.LinearLayoutCompat;

import java.util.ArrayList;
import java.util.List;

import pt.uc.dei.copmode.npm.R;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;
import pt.uc.dei.copmode.npm.services.SetupCheckService;
import pt.uc.dei.copmode.npm.services.PermissionManagerService;

public class SetupNPMActivity extends AppCompatActivity {
    private static final String TAG = "SetupNPMActivity";

    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int REQUEST_WIFI_SCAN_ALWAYS_AVAILABLE_CODE = 2;

    private LinearLayoutCompat packageUsageContent;
    private LinearLayoutCompat permissionRequestContent;
    private LinearLayoutCompat alwaysScanContent;
    private TextView statusTextView;
    private AppCompatButton exitButton;

    private boolean shouldRequestPackageUsage = false;
    private String[] remainingRequestPermissions = null;

    private WifiManager wifiManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_npm);
        packageUsageContent = findViewById(R.id.package_usage_content);
        permissionRequestContent = findViewById(R.id.permissions_content);
        alwaysScanContent = findViewById(R.id.always_scan_content);
        statusTextView = findViewById(R.id.status_info);

        wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);

        Intent intent = getIntent();
        List<String> missingPermissions = intent.getStringArrayListExtra(SetupCheckService.EXTRA_MISSING_PERMISSIONS_KEY);

        if ((missingPermissions == null || missingPermissions.isEmpty()) && !shouldRequestWifiScanAlwaysAvailable()) {
            MyLogHelper.logError(TAG, TAG + " was called with empty intent. Exiting...");
            finish();
            return;
        }

        if (missingPermissions != null && !missingPermissions.isEmpty()) {
            if (missingPermissions.contains(Manifest.permission.PACKAGE_USAGE_STATS)) {
                shouldRequestPackageUsage = true;
                missingPermissions.remove(Manifest.permission.PACKAGE_USAGE_STATS);
            }
            remainingRequestPermissions = missingPermissions.toArray(new String[0]);
        }

        AppCompatButton requestPackageUsageButton = findViewById(R.id.request_package_usage_button);

        requestPackageUsageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                startActivity(i);
            }
        });

        AppCompatButton requestPermissionsButton = findViewById(R.id.request_permissions_button);
        requestPermissionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (remainingRequestPermissions != null && remainingRequestPermissions.length > 0)
                    requestPermissions(remainingRequestPermissions, PERMISSION_REQUEST_CODE);
            }
        });

        AppCompatButton requestAlwaysScanButton = findViewById(R.id.request_always_scan_button);
        requestAlwaysScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shouldRequestWifiScanAlwaysAvailable()) {
                    Intent i = new Intent(WifiManager.ACTION_REQUEST_SCAN_ALWAYS_AVAILABLE);
                    SetupNPMActivity.this.startActivityForResult(i, REQUEST_WIFI_SCAN_ALWAYS_AVAILABLE_CODE);
                }
            }
        });

        exitButton = findViewById(R.id.close_setup_button);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_WIFI_SCAN_ALWAYS_AVAILABLE_CODE) {
            // we do nothing because on resume is called and will update UI in accordance
            return; // note that we need to call startActivityForResult, or the prompt will display null instead of CM-NPM in the app name
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateViews();
    }

    void updateViews() {
        String status = "";
        if (remainingRequestPermissions == null || remainingRequestPermissions.length == 0) {
            permissionRequestContent.setVisibility(View.GONE);
        } else {
            status += getString(R.string.some_permissions_not_granted) + ' ';
        }

        if (shouldRequestPackageUsage) {
            if (PermissionManagerService.hasUsageAccessPermission(this)) {
                // we got the permission
                shouldRequestPackageUsage = false;
                packageUsageContent.setVisibility(View.GONE);
            } else {
                status += getString(R.string.package_usage_permission_not_granted) + ' ';
            }
        } else {
            packageUsageContent.setVisibility(View.GONE);
        }

        if (shouldRequestWifiScanAlwaysAvailable()) {
            status += getString(R.string.always_scan_not_granted);
        } else {
            alwaysScanContent.setVisibility(View.GONE);
        }

        if (status.isEmpty()) {
            statusTextView.setText(R.string.all_permissions_granted);
            exitButton.setVisibility(View.VISIBLE);
        } else {
            status += '\n' + getString(R.string.follow_instructions_below);
            statusTextView.setText(status);
            exitButton.setVisibility(View.GONE);
        }
    }

    private boolean shouldRequestWifiScanAlwaysAvailable() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            return !wifiManager.isScanAlwaysAvailable();
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length == 0) {
            // the request was cancelled. The user should request again
            return;
        }

        if (requestCode == PERMISSION_REQUEST_CODE) {
            List<String> deniedPermissions = new ArrayList<>();
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    deniedPermissions.add(permissions[i]);
                }
            }
            remainingRequestPermissions = deniedPermissions.toArray(new String[0]); // can be empty
        }
        updateViews();
    }

    @Override
    public void finish() {
        Intent serviceIntent = new Intent(getApplicationContext(), PermissionManagerService.class);
        serviceIntent.setAction(PermissionManagerService.ACTION_RESTART_SERVICES);
        startService(serviceIntent);  // send intent to service to restart the services.
        super.finish();
    }
}
