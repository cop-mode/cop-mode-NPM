package pt.uc.dei.copmode.npm.helpers;

import android.util.Log;

public class MyLogHelper {
    public static void logInfo(String tag, String msg){
        Log.i(tag, msg);
    }

    public static void logDebug(String tag, String msg){
        Log.d(tag, msg);
    }

    public static void logWarn(String tag, String msg){
        Log.w(tag, msg);
    }

    public static void logError(String tag, String msg){
        Log.e(tag, msg);
    }

    public static void logException(Exception e){
        e.printStackTrace();
    }
}
