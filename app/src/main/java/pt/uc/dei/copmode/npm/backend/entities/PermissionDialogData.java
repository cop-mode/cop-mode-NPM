package pt.uc.dei.copmode.npm.backend.entities;

import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Objects;

import pt.uc.dei.copmode.npm.annotations.ExcludeFromUpload;
import pt.uc.dei.copmode.npm.backend.database.PermissionDialogDataDao;

@Entity(tableName = PermissionDialogDataDao.PERMISSION_DIALOG_DATA_TABLE)
public class PermissionDialogData {
    @Ignore
    private static final String TAG = "PermissionDialogData";
    @Ignore
    public static final String INTENT_KEY = "permission_dialog_data_extra";

    @Ignore
    // A dialog response is valid for 30 minutes.
    public static final long EXPIRE_INTERVAL_MILLIS = 1800000;

    @PrimaryKey(autoGenerate = true)
    private long permissionDialogDataId;  // only used for the DB -- must be public or have a getter/setter/

    public long getPermissionDialogDataId() {
        return permissionDialogDataId;
    }

    @ExcludeFromUpload
    public int threadId = -1;  // this is only used in the hook handler
    @Ignore
    private static final String threadIdKey = "thread_id_key";

    @NonNull
    @Embedded(prefix = "requesting_")
    public RunningApplicationInfo requestingApplicationInfo;
    @Ignore
    private static final String requestingApplicationInfoKey = "requesting_application_info_key";

    @Nullable
    @Embedded(prefix = "ipc_calling_")
    public RunningApplicationInfo ipcCallingApplicationInfo;
    @Ignore
    private static final String ipcCallingApplicationInfoKey = "ipc_calling_application_info_key";

    @NonNull
    public String checkedPermission;
    @Ignore
    private static final String checkedPermissionKey = "checked_permission_key";

    @Nullable
    public String checkedPermissionGroup;
    @Ignore
    private static final String checkedPermissionGroupKey = "checked_permission_group_key";

    @NonNull
    public String method;
    @Ignore
    private static final String methodKey = "method_key";

    @NonNull
    public long timestamp;  // milliseconds
    @Ignore
    private static final String timestampKey = "timestamp_key";

    @NonNull
    public PermissionDialogAnswerType answerType = PermissionDialogAnswerType.USER_ANSWERED;
    @Ignore
    private static final String answerTypeKey = "answer_type_key";

    public int grantResult = Integer.MIN_VALUE;  // start with "non-set" value
    @Ignore
    private static final String grantResultKey = "grant_result_key";

    public String selectedSemanticLoc;
    @Ignore
    private static final String selectedSemanticLocKey = "selected_semantic_loc_key";

    public int wasRequestExpected = Integer.MIN_VALUE;
    @Ignore
    private static final String wasRequestExpectedKey = "was_request_expected_key";

    @Ignore
    public PermissionDialogData(@NonNull RunningApplicationInfo requestingApplicationInfo,
                                @Nullable RunningApplicationInfo ipcCallingApplicationInfo,
                                @NonNull String checkedPermission,
                                @Nullable String checkedPermissionGroup,
                                @NonNull String method,
                                int threadId) {
        this.requestingApplicationInfo = new RunningApplicationInfo(requestingApplicationInfo);
        if (ipcCallingApplicationInfo != null)
            this.ipcCallingApplicationInfo = new RunningApplicationInfo(ipcCallingApplicationInfo);
        this.checkedPermission = checkedPermission;
        this.checkedPermissionGroup = checkedPermissionGroup;
        this.method = method;
        this.timestamp = System.currentTimeMillis();
        this.answerType = PermissionDialogAnswerType.UNANSWERED;
        this.threadId = threadId;
    }

    // Room will use this constructor when fetching from the DB
    public PermissionDialogData(long permissionDialogDataId) {
        this.permissionDialogDataId = permissionDialogDataId;
    }

    /**
     * Returns a new PermissionDialogData that is a copy of this object, with permissionDialogDataId not set,
     * the timestamp set for the current timetstamp and answerType set to PermissionDialogAnswerType.CACHE_ANSWERED.
     * @param method the method called to check the permission as it might differ from the cached value.
     * @param threadId the threadId of the current checking permission thread
     * @return the copy
     */
    public PermissionDialogData createCacheCopy(String method, int threadId) {
        PermissionDialogData copy = new PermissionDialogData(requestingApplicationInfo, ipcCallingApplicationInfo, checkedPermission, checkedPermissionGroup, method, threadId);
        copy.answerType = PermissionDialogAnswerType.CACHE_ANSWERED;
        copy.grantResult = grantResult;
        copy.selectedSemanticLoc = selectedSemanticLoc;
        copy.wasRequestExpected = wasRequestExpected;
        return copy;
    }

    public Bundle toBundle() {
        // Initially this class implemented parcelable, to be passed around processes. However,
        // in xposed I was getting this error: E/Parcel: Class not found when unmarshalling: pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData
        // Caused by: java.lang.NoClassDefFoundError: Class not found using the boot class loader; no stack trace available
        // See cause: https://developer.android.com/reference/android/os/Message#obj
        // I couldn't fix it. So I could either use Serializable or use Bundles. Bundles seem more efficient.
        Bundle bundle = new Bundle();
        bundle.putInt(threadIdKey, threadId);
        bundle.putBundle(requestingApplicationInfoKey, requestingApplicationInfo.toBundle());
        bundle.putBundle(ipcCallingApplicationInfoKey, ipcCallingApplicationInfo != null ? ipcCallingApplicationInfo.toBundle() : null);
        bundle.putString(checkedPermissionKey, checkedPermission);
        bundle.putString(checkedPermissionGroupKey, checkedPermissionGroup);
        bundle.putString(methodKey, method);
        bundle.putLong(timestampKey, timestamp);
        bundle.putInt(answerTypeKey, answerType.getValue());
        bundle.putInt(grantResultKey, grantResult);
        bundle.putString(selectedSemanticLocKey, selectedSemanticLoc);
        bundle.putInt(wasRequestExpectedKey, wasRequestExpected);
        return bundle;
    }

    public static PermissionDialogData fromBundle(Bundle bundle) {
        RunningApplicationInfo requestingApplicationInfo = Objects.requireNonNull(RunningApplicationInfo.fromBundle(bundle.getBundle(requestingApplicationInfoKey)));
        RunningApplicationInfo ipcCallingApplicationInfo = RunningApplicationInfo.fromBundle(bundle.getBundle(ipcCallingApplicationInfoKey));
        String checkedPermission = Objects.requireNonNull(bundle.getString(checkedPermissionKey));
        String checkedPermissionGroup = bundle.getString(checkedPermissionGroupKey);
        String method = Objects.requireNonNull(bundle.getString(methodKey));
        int threadId = bundle.getInt(threadIdKey);
        PermissionDialogData data = new PermissionDialogData(requestingApplicationInfo, ipcCallingApplicationInfo, checkedPermission, checkedPermissionGroup, method, threadId);
        data.answerType = PermissionDialogAnswerType.fromValue(bundle.getInt(answerTypeKey));
        data.timestamp = bundle.getLong(timestampKey);
        data.grantResult = bundle.getInt(grantResultKey);
        data.selectedSemanticLoc = bundle.getString(selectedSemanticLocKey);
        data.wasRequestExpected = bundle.getInt(wasRequestExpectedKey);
        return data;
    }

    public static long getNonExpiredStartTimestamp() {
        return System.currentTimeMillis() - EXPIRE_INTERVAL_MILLIS;
    }

    @NonNull
    @Override
    public String toString() {
        return "PermissionDialogData(" + requestingApplicationInfo.packageName + ',' + checkedPermission + ',' + checkedPermissionGroup + ',' + timestamp + ',' + grantResult + ')';
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof PermissionDialogData))
            return false;

        PermissionDialogData dataObj = (PermissionDialogData) obj;
        // we do not use the id because that is set by the database and it is not set before insertion.
        return requestingApplicationInfo.packageName.equals(dataObj.requestingApplicationInfo.packageName) &&
                checkedPermission.equals(dataObj.checkedPermission) && timestamp == dataObj.timestamp;
    }

    public boolean wasGranted() {
        return grantResult == PackageManager.PERMISSION_GRANTED;
    }

    public void setUnhandled() {
        answerType = PermissionDialogAnswerType.UNHANDLED;
    }
}
