package pt.uc.dei.copmode.npm.xposed;

import androidx.annotation.NonNull;

import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;

class XCachedPermissionResult {
    String checkedPermission;
    long timestamp;
    int grantResult;

    XCachedPermissionResult(String checkedPermission, long timestamp, int grantResult) {
        // reduced version of PermissionDialogData to save memory
        this.checkedPermission = checkedPermission;
        this.timestamp = timestamp;
        this.grantResult = grantResult;
    }

    XCachedPermissionResult(PermissionDialogData permissionDialogData) {
        checkedPermission = permissionDialogData.checkedPermission;
        timestamp = permissionDialogData.timestamp;
        grantResult = permissionDialogData.grantResult;
    }

    boolean stillValid() {
        return (timestamp >= System.currentTimeMillis() - PermissionDialogData.EXPIRE_INTERVAL_MILLIS);
    }

    @NonNull
    @Override
    public String toString() {
        return "XCachedPermissionResult(" + checkedPermission + ", " + timestamp + ", " + grantResult + ")";
    }
}
