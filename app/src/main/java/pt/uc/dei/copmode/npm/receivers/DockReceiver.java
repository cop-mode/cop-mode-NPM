package pt.uc.dei.copmode.npm.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import pt.uc.dei.copmode.npm.components.ContextualDataHolder;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

public class DockReceiver extends BroadcastReceiver implements ContextReceiver {
    private final static String TAG = "DockReceiver";
    private boolean isReceiving = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null || !action.equals(Intent.ACTION_DOCK_EVENT))
            return;

        MyLogHelper.logDebug(TAG, "Dock state changed");

        int dockState = intent.getIntExtra(Intent.EXTRA_DOCK_STATE, -1);
        if (dockState != -1)
            ContextualDataHolder.getInstance(context).updateDockState(dockState);
    }

    @Override
    public boolean registerSelf(Context context) {
        IntentFilter filter = new IntentFilter(Intent.ACTION_DOCK_EVENT);
        context.registerReceiver(this, filter);
        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }
}
