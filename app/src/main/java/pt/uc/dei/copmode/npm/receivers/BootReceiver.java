package pt.uc.dei.copmode.npm.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import pt.uc.dei.copmode.npm.helpers.MyLogHelper;
import pt.uc.dei.copmode.npm.services.PermissionManagerService;

public class BootReceiver extends BroadcastReceiver {
    private static String TAG = "BootReceiver";

    public void onReceive(Context context, Intent arg1)
    {
        MyLogHelper.logDebug(TAG, "Received Boot broadcast. Starting PermissionManagerService");
        PermissionManagerService.startServiceIfOff(context);
    }
}
