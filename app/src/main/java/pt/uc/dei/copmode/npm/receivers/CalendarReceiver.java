package pt.uc.dei.copmode.npm.receivers;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import pt.uc.dei.copmode.npm.backend.entities.CalendarEvent;
import pt.uc.dei.copmode.npm.components.ContextualDataHolder;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

public class CalendarReceiver extends BroadcastReceiver implements ContextReceiver {
    private static final String TAG = "CalendarReceiver";
    private boolean isReceiving = false;

    private static final long SAMPLE_INTERVAL_MILLIS = 14400000;  // every 4 hours or when there's a change in the calendar
    // we get just a portion of the calendar as a cache, such that searches on that portion are faster (for retrieving on going events)

    private CalendarChangeTimerTask calendarChangeTimerTask;

    private static final String[] PROJECTION = {
            CalendarContract.Instances._ID,
            CalendarContract.Instances.EVENT_ID,
            CalendarContract.Instances.CALENDAR_ID,
            CalendarContract.Instances.BEGIN,
            CalendarContract.Instances.END,
            CalendarContract.Instances.EVENT_LOCATION,
            CalendarContract.Instances.STATUS,
    };

    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_EVENT_ID_INDEX = 1;
    private static final int PROJECTION_CALENDAR_ID_INDEX = 2;
    private static final int PROJECTION_BEGIN_INDEX = 3;
    private static final int PROJECTION_END_INDEX = 4;
    private static final int PROJECTION_EVENT_LOCATION_INDEX = 5;
    private static final int PROJECTION_STATUS_INDEX = 6;

    @Override
    public void onReceive(Context context, Intent intent) {
        MyLogHelper.logDebug(TAG, "Received calendar intent");
        if (intent.getAction() != null && intent.getAction().equals(Intent.ACTION_PROVIDER_CHANGED)) {
            if (intent.getData().equals(CalendarContract.CONTENT_URI)) {
                if (calendarChangeTimerTask != null) {
                    calendarChangeTimerTask.run();
                }
            }
        }
    }

    @Override
    public boolean registerSelf(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            MyLogHelper.logDebug(TAG, "Failed to start " + TAG + " due to missing permission");
            isReceiving = false;
            return false;
        }

        IntentFilter filter = new IntentFilter(Intent.ACTION_PROVIDER_CHANGED);
        filter.addDataScheme("content");
        filter.addDataAuthority(CalendarContract.AUTHORITY, null);
        context.registerReceiver(this, filter);

        // schedule to periodically retrieve a portion of the calendar
        calendarChangeTimerTask = new CalendarChangeTimerTask(context);
        Timer timer = new Timer();
        timer.schedule(calendarChangeTimerTask, 0, SAMPLE_INTERVAL_MILLIS);

        isReceiving = true;
        return true;
    }

    @Override
    public boolean isReceiving() {
        return isReceiving;
    }

    private static class CalendarChangeTimerTask extends TimerTask {
        private Context context;
        CalendarChangeTimerTask(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            MyLogHelper.logDebug(TAG, "Updating calendar data");
            // get events in the next time frame (from now up to SAMPLE_INTERVAL_MILLIS in the future)
            long currentTime = System.currentTimeMillis();
            long futureTime = currentTime + SAMPLE_INTERVAL_MILLIS;

            Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
            ContentUris.appendId(builder, currentTime);
            ContentUris.appendId(builder, futureTime);

            ContentResolver cr = context.getContentResolver();
            Cursor cursor = cr.query(builder.build(),
                    PROJECTION,
                    null,
                    null,
                    getDefaultOrderBy());

            if (cursor == null) {
                MyLogHelper.logWarn(TAG, "Received null cursor from calendar instances provider. Failed to retrieve events");
                return;
            }

            List<CalendarEvent> calendarEvents = new ArrayList<>(cursor.getCount());
            while (cursor.moveToNext()) {
                long instanceId = cursor.getLong(PROJECTION_ID_INDEX);
                long calendarId = cursor.getLong(PROJECTION_CALENDAR_ID_INDEX);
                long eventId = cursor.getLong(PROJECTION_EVENT_ID_INDEX);
                long eventBegin = cursor.getLong(PROJECTION_BEGIN_INDEX);
                long eventEnd = cursor.getLong(PROJECTION_END_INDEX);
                String eventLocation = cursor.getString(PROJECTION_EVENT_LOCATION_INDEX);
                int eventStatus = cursor.getInt(PROJECTION_STATUS_INDEX);

                calendarEvents.add(new CalendarEvent(instanceId, calendarId, eventId, eventBegin, eventEnd, eventLocation, eventStatus));
            }
            cursor.close();
            ContextualDataHolder.getInstance(context).updateCalendarEventsCache(calendarEvents);
        }

        static String getDefaultOrderBy() {
            return CalendarContract.Instances.BEGIN + " DESC";
        }
    }
}
