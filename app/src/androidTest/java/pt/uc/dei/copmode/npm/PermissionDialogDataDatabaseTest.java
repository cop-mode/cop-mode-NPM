package pt.uc.dei.copmode.npm;

import android.content.Context;

import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import pt.uc.dei.copmode.npm.backend.database.NPMDatabase;
import pt.uc.dei.copmode.npm.backend.database.PermissionDialogDataDao;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAnswerType;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

@RunWith(AndroidJUnit4.class)
public class PermissionDialogDataDatabaseTest {
    private static final String TAG = "PermissionDialogDataDatabaseTest";
    private PermissionDialogDataDao dataDao;
    private NPMDatabase db;

    @Before
    public void createDb() {
        // Context appContext = ApplicationProvider.getApplicationContext();
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        db = Room.inMemoryDatabaseBuilder(appContext, NPMDatabase.class).build();
        dataDao = db.permissionDialogDataDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void writeData() throws Exception {
        String appName = "appNamePartialTest";
        String pckgName = "pckgNamePartialTest";
        String checkedPermission = "checkedPermissionPartialTest";
        PermissionDialogData data = new PermissionDialogData(appName, pckgName, checkedPermission);
        dataDao.insertPermissionDialogData(data);
        PermissionDialogData data2 = new PermissionDialogData(appName, pckgName, checkedPermission);
        dataDao.insertPermissionDialogData(data2);
        PermissionDialogData[] dataFromDb = dataDao.getOrderedFilteredPermissionDialogDataByPermission(pckgName, checkedPermission, PermissionDialogData.getNonExpiredStartTimestamp(), PermissionDialogAnswerType.UNANSWERED);
        assertEquals(dataFromDb[0].pckgName, data.pckgName);
        assertEquals(dataFromDb[1].pckgName, data.pckgName);
        MyLogHelper.logDebug(TAG, "" + dataFromDb[0].toString());
        MyLogHelper.logDebug(TAG, "" + dataFromDb[1].toString());
    }

}