package pt.uc.dei.copmode.npm;

import android.content.Context;
import android.util.Log;

import androidx.room.Room;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import pt.uc.dei.copmode.npm.backend.database.ContextDataDao;
import pt.uc.dei.copmode.npm.backend.database.NPMDatabase;
import pt.uc.dei.copmode.npm.backend.database.PermissionDialogDataDao;
import pt.uc.dei.copmode.npm.backend.entities.ContextData;
import pt.uc.dei.copmode.npm.backend.entities.MyBluetoothDevice;
import pt.uc.dei.copmode.npm.backend.entities.NetworkStatus;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogAndContextData;
import pt.uc.dei.copmode.npm.backend.entities.PermissionDialogData;
import pt.uc.dei.copmode.npm.backend.entities.WifiDevice;
import pt.uc.dei.copmode.npm.helpers.MyLogHelper;

import static org.junit.Assert.assertEquals;

public class ContextDataTest {
    private static final String TAG = "ContextDataTest";
    private PermissionDialogDataDao dialogDataDao;
    private ContextDataDao contextDataDao;
    private NPMDatabase db;

    @Before
    public void createDb() {
        // Context appContext = ApplicationProvider.getApplicationContext();
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        db = Room.inMemoryDatabaseBuilder(appContext, NPMDatabase.class).allowMainThreadQueries().build();
        dialogDataDao = db.permissionDialogDataDao();
        contextDataDao = db.contextDataDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void writeData() throws Exception {
        String appName = "appNamePartialTest";
        String pckgName = "pckgNamePartialTest";
        String checkedPermission = "checkedPermissionPartialTest";
        PermissionDialogData permissionData = new PermissionDialogData(appName, pckgName, checkedPermission);
        long dialogDataId = dialogDataDao.insertPermissionDialogData(permissionData);
        MyLogHelper.logDebug(TAG, "dialogDataId: " + dialogDataId);

        ContextData contextData = new ContextData();
        contextData.permissionDialogDataOwnerId = dialogDataId;
        contextData.networkStatus = NetworkStatus.METERED;
        int nrFakeDevices = 5;
        contextData.wifiDevices = getFakeWifiDevices(nrFakeDevices);
        contextData.bluetoothDevices = getFakeBluetoothDevices(nrFakeDevices);
        MyLogHelper.logDebug(TAG, contextData.wifiDevices.toString());

        long contextDataId = contextDataDao.insertContextData(contextData);
        MyLogHelper.logDebug(TAG, "contextDataId = " + contextDataId);

        List<PermissionDialogAndContextData> dataFromDb = dialogDataDao.getAllPermissionDialogAndContextData();
        assertEquals(dataFromDb.size(), 1);
        PermissionDialogAndContextData elem = dataFromDb.get(0);
        assertEquals(elem.permissionDialogData.getPermissionDialogDataId(), elem.contextData.permissionDialogDataOwnerId);
        assertEquals(elem.contextData.networkStatus, NetworkStatus.METERED);
        assertEquals(elem.contextData.wifiDevices.size(), nrFakeDevices);
        assertEquals(elem.contextData.bluetoothDevices.size(), nrFakeDevices);
        // MyLogHelper.logDebug(TAG, elem.contextData.wifiDevices.toString());
    }

    public List<WifiDevice> getFakeWifiDevices(int count) {
        List<WifiDevice> list = new ArrayList<>(count);
        Random rnd = new Random();
        for (int i=0; i<count; i++) {
            list.add(new WifiDevice(UUID.randomUUID().toString(), UUID.randomUUID().toString(), rnd.nextInt(), rnd.nextLong()));
        }
        return list;
    }

    public List<MyBluetoothDevice> getFakeBluetoothDevices(int count) {
        List<MyBluetoothDevice> list = new ArrayList<>(count);
        Random rnd = new Random();
        for (int i=0; i<count; i++) {
            list.add(new MyBluetoothDevice("test_device", "F8:77:B8:62:2B:9B", 0, 0, 0, (short) 0, 0));
        }
        return list;
    }
}
