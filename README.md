This repository contains the code for COP-MODE Naive Permission Manager (CM-NPM).

CM-NPM uses the Xposed framework, so to test this, make sure you have Xposed or EdXposed installed.
Changes to the hooks require rebooting.

Example of a permission prompt:

![CM-NPM Permission Prompt](telegram_storage_request.png)


## Implementation details

To intercept permission requests we could have either changed the operating system, use the 
accessibility features to read permission requests [1] or to require 
root [2]. We chose the latter as it is easier to setup and maintain and 
supports multiple operating system versions.

To intercept the operating system API calls, we implemented NPM as an EdXposed module [3]. 
EdXposed is a fork of the Xposed framework [4] to support newer Android versions (the Xposed framework supports from Android 4.0.3 (API level 15) up to Android 8.1 (API level 27) [5] , 
while EdXposed supports from Android 8.0 (API level 26) up to Android 11 (API level 30) [3]). 
Xposed allows modules to change the behavior of other apps or the system itself without changing any 
APKs [4], by adding an additional library (Xposed) to _app\_process_, the process 
that spawns every application. Xposed then allows modules to hook (intercept) API calls, 
including the OS, and therefore modify the functionally before and after API execution [6].

The work in [2] focused on hooking API calls that collected data. 
This approach worked well at the time as it targeted Android API versions that were before Android 
supported runtime permissions. With runtime permissions, apps need to check if they have a permission 
before executing the API call that would collect the data.
If the permission is denied (either previously denied by the user, or never requested), the app may 
or may not request the permission [7], as it can check whether it has
the permission without explicitly asking the user.
This conditional execution led us to primarily hook \textit{permission checks}, escalating them to 
permission requests in the form of the prompts illustrated in the figure above, 
while intercepting \textit{permission requests} only to override the result with the participant 
input given to the prompt created by NPM at the respective permission check. 
This latter interception allow us to bypass the Android default permission manager.


In Android, permissions can have different protection levels and only \textit{dangerous permissions} 
require an explicit request by applications [7]. These permissions 
are considered ``dangerous" because they allow apps to access sensitive data or resources that can 
affect the system and/or the privacy of the user. Therefore, NPM only handles, and therefore 
collects, permission requests related to dangerous permissions. Furthermore, because the default 
Android permission manager manages the permissions at a group level, a controversial implementation 
decision [8], so does NPM. That is, by default, if, for instance, a 
permission requires the read calendar permission and the user grants it, the app will automatically 
be granted the write calendar permission on request. While one can argue about this feature from a 
privacy perspective, NPM follows this behavior in order to replicate Android's permission manager 
from a data collection point of view.

When a dangerous permission check call is made by an app, NPM prompts the user as illustrated in 
the figure above and collects the contextual data aforementioned. 
Similarly to the work in [2], we cache the answer for 30 minutes, thus 
returning the same answer for the given app and permission group for this duration, in order to 
avoid warning fatigue [9].
The permission icon and permission description are obtained directly from the Android operating 
system, so as to not bias the response. To avoid breaking functionality, NPM does not handle 
permission requests from system apps, letting the Android native permission manager handle those.

## Bypassing Android's Permission Manager

Runtime permissions resort to essentially 2 groups of functions: `checkPermission` and `requestPermissions`.
The `checkPermission` functions check if the app has a given permission and can be called from any component.
`requestPermissions` can request multiple permissions and be called from any activity context, where context has a graphical interface 
(e.g. services can't request permissions, only check).

Even though I'm hooking these 2 sets of functions, I'm not yet bypassing the permission manager, because the permissions are stored somewhere and
only changing these values would effectively bypass. 
As a work-around to bypass Android's permission manager, we will set all permissions as granted when installing the personal apps in the phones (with ADB). 
However, the users might change this in the settings, potentially resulting in two permission prompts: ours and Android's. 
TODO: See what we can do about this.

## Permission Prompt (outdated)

For prompting from an hooked context, there are essentially 3 approaches:

1. If we have an activity context (which we do when hooking `requestPermissions()`), we can show an alert dialog from such context and this is the current implementation. However,
we have the activity context because we are hooking `requestPermissions()` which requires an activity. However, services may check for permissions but not require them.
Thus, if a service sees a permission denied, it may start an activity to request the permission, resulting in a change in the device context. That is,
getting the device context in `requestPermissions()` is already a different device context when the application called `checkPermission()`.
We could potentially link the `requestPermissions()` to a previous call to `checkPermission()` (and hence the context).
However, a call to `checkPermission()` does not always lead to a call to `requestPermissions()`.
Additionally, multiple `checkPermission()` might be performed, before a `requestPermission()`. Thus the heuristic could be something like: in a `requestPermissions()`,
check the calls `checkPermission()` for the same app containing the requested permission in last 1 minute (or so). If there's a match, then link both requests.
   1. This approach has the disadvantage that the layout inherits the style from the application requesting the permission. Thus, the prompt might vary from app to app.
   This might be mitigated by fully stylizing the layout.

2. Use the application context to show a system alert.
    This solution works, but requires permission to draw over other apps. (implemented in `PermissionManagerService.showPermissionPromptAsSystemDialog()`)
Additionally, it won't work in Android 10 and onwards as it requires SYSTEM_ALERT_WINDOW:
[https://developer.android.com/about/versions/10/behavior-changes-all#sysalert-go](https://developer.android.com/about/versions/10/behavior-changes-all#sysalert-go)

3. Hook in `checkPermissions()` and use the application context to contact `PermissionManagerService` to start `PermissionDialogActivity` and to collect the contextual data.
This activity will prompt the user while returning the data to the service. The service then forwards the user decision to the `checkPermissions()` call.
This approach has the disadvantage that the context in `checkPermissions()`, which might be the main ui, will be locked during the prompt and until a response is obtained.
This might result in "Application not Responding" (ANR) -- probably not, since our activity will become the foreground.
   1. We cannot use aidl to communicate from the app to the service and vice-versa, since the application does not have access to the aidl. From [https://developer.android.com/guide/components/aidl](https://developer.android.com/guide/components/aidl):

            The client must also have access to the interface class, so if the client and service are in separate applications, then the client's application must have a copy of the .aidl file in its src/ directory (which generates the android.os.Binder interface—providing the client access to the AIDL methods).
        I tested this and it does not work as apps have no access to the aidl. So we probably have to use the class [Messenger](https://developer.android.com/guide/components/bound-services#Messenger) for two-way communication.

We went with option 3.


## References

1. H. Fu, Z. Zheng, S. Zhu, and P. Mohapatra, "Keeping context in mind:
Automating mobile app access control with user interface inspection," in
IEEE INFOCOM 2019-IEEE Conference on Computer Communications.
IEEE, 2019, pp. 2089–2097.
2. K. Olejnik, I. I. Dacosta Petrocelli, J. C. Soares Machado, K. Huguenin,
M. E. Khan, and J.-P. Hubaux, “Smarper: Context-aware and automatic
runtime-permissions for mobile devices,” in Proceedings of the 38th
IEEE Symposium on Security and Privacy (SP), no. EPFL-CONF-226751. IEEE, 2017.
3. ElderDrivers, Edxposed framework," https://github.com/ElderDrivers/EdXposed, accessed: 2021-08-29
4. rovo89, "Xposed Installer – Xposed Module Repository," https://repo.xposed.info/module/de.robv.android.xposed.installer, accessed: 2021-08-29.
5. rovo89, "[OFFICIAL] Xposed for Lollipop/Marshmallow/Nougat/Oreo
[v90-beta3, 2018/01/29]," https://forum.xda-developers.com/t/official-xposed-for-lollipop-marshmallow-nougat-oreo-v90-beta3-2018-01-29.3034811/, 2018, accessed: 2021-08-29.
6. rovo89, "Xposed Development Tutorial", https://github.com/rovo89/XposedBridge/wiki/Development-tutorial, accessed: 2021-08-29.
7. G. Developers, "Request app permissions," https://developer.android.com/training/permissions/requesting, accessed: 2021-08-29.
8. P. Calciati, K. Kuznetsov, A. Gorla, and A. Zeller, “Automatically granted permissions in android apps: An empirical study on their prevalence and on the potential threats for privacy,” in Proceedings of the 17th International Conference on Mining Software Repositories, ser. MSR ’20. New York, NY, USA: Association for Computing Machinery, 2020, p. 114–124.
9. A. Felt, E. Ha, S. Egelman, and A. Haney, “Android permissions: User
attention, comprehension, and behavior,” Proc. of SOUPS, pp. 1–14, 2012.